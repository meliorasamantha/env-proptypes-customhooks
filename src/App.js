import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Container } from '@mui/material';
import Card from './components/Card';
import Navbar from './components/Navbar';

function App() {
  return (
    <Router>
      <div className="app">
        <Navbar />
        <Container maxWidth="sm" sx={{ mt: 2 }}>
          <Routes>
            <Route path="/" element={<Card title="Main Content" 
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed placerat, turpis ut consequat elementum, nisl justo venenatis nulla, ut tristique felis elit vel ex. Mauris eleifend risus in mi auctor, vitae fermentum mi volutpat. Morbi a sagittis leo. Maecenas eu augue luctus, pretium risus in, iaculis mauris. Nunc non eros id justo lobortis aliquam. In sit amet consequat est. Cras mattis leo vitae varius pulvinar. Donec a nunc elit. In hac habitasse platea dictumst. Nulla bibendum, sem eu venenatis tristique, ante felis pellentesque erat, vel euismod ex ex at quam. Ut mollis vulputate leo non vestibulum.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dapibus lorem, et convallis tellus malesuada a. Integer efficitur viverra odio, id sollicitudin purus. Nullam nec est turpis. Quisque a nisl sit amet justo bibendum ullamcorper. Cras at varius sem. Mauris commodo rutrum libero, nec sagittis nisi bibendum in. In hac habitasse platea dictumst. Integer sollicitudin eu mauris sit amet viverra. Quisque eu augue sed mi elementum viverra. Etiam dapibus, turpis non viverra commodo, ante elit maximus sem, eget ultrices turpis turpis ac elit. Etiam vel augue at justo commodo iaculis. Vivamus convallis orci sed nulla rhoncus malesuada. Integer volutpat ex lectus, eget aliquam felis rhoncus vitae. Curabitur iaculis nibh sit amet mi varius, nec posuere dui ullamcorper. Sed condimentum risus vitae laoreet vestibulum. Sed ac neque eget tellus consectetur consectetur." />} />
            <Route path="/add-user" element={<Card title="Add User" content="Add user form goes here." />} />
            <Route path="/admin" element={<Card title="Admin" content="Admin panel goes here." />} />
          </Routes>
        </Container>
      </div>
    </Router>
  );
}

export default App;
