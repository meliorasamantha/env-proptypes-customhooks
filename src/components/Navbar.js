import React from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Typography, Button } from '@mui/material';

function Navbar() {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          MyApp
        </Typography>
        <Button component={Link} to="/" color="inherit">
          Home
        </Button>
        <Button component={Link} to="/add-user" color="inherit">
          Add User
        </Button>
        <Button component={Link} to="/admin" color="inherit">
          Admin
        </Button>
      </Toolbar>
    </AppBar>
  );
}

export default Navbar;
